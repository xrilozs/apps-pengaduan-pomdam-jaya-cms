<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->helper('url_helper');
  }
  
  public function index(){
    redirect('/dashboard', 'refresh');
  }

  public function login(){    
    $data['page'] = "login";
    $data['version'] = date("YmdHis");
		$this->load->view('pages/home/login', $data);
  }
  
  public function dashboard(){    
    $data['page'] = "dashboard";
    $data['version'] = date("YmdHis");
		$this->load->view('layouts/main_header', $data);
		$this->load->view('layouts/main_sidebar', $data);
		$this->load->view('pages/home/dashboard');
		$this->load->view('layouts/main_footer', $data);
  }
}