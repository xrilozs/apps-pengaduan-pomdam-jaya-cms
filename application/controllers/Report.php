<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->helper('url_helper');
  }
  
  public function page(){    
    $data['page']     = "Report";
    $data['version']  = date("YmdHis");

		$this->load->view('layouts/main_header', $data);
		$this->load->view('layouts/main_sidebar', $data);
		$this->load->view('pages/report/page');
		$this->load->view('layouts/main_footer', $data);
  }

  public function detail(){
    $data['page']     = "Report Detail";
    $data['version']  = date("YmdHis");

		$this->load->view('layouts/main_header', $data);
		$this->load->view('layouts/main_sidebar', $data);
		$this->load->view('pages/report/detail');
		$this->load->view('layouts/main_footer', $data);
  }
}