<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->helper('url_helper');
  }
  
  public function page(){    
    $data['page']     = "User";
    $data['version']  = date("YmdHis");

		$this->load->view('layouts/main_header', $data);
		$this->load->view('layouts/main_sidebar', $data);
		$this->load->view('pages/user/page');
		$this->load->view('layouts/main_footer', $data);
  }
}