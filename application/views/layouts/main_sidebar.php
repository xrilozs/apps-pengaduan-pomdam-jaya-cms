  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-success elevation-2">
    <!-- Brand Logo -->
    <a href="<?=BASE_URL;?>dashboard" class="brand-link">
      <img src="<?=ASSETS;?>img/web/logo_sc2.png" alt="AdminLTE Logo" class="brand-image" style="opacity: .8">
      <span class="brand-text font-weight-bold">Pomdam Jaya</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item hidden-menu" id="sidebar-dashboard-menu">
            <a href="<?=BASE_URL;?>dashboard" class="nav-link <?=$page == 'dashboard' ? 'active' : '';?>">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item hidden-menu" id="sidebar-report-menu">
            <a href="<?=BASE_URL;?>report" class="nav-link <?=$page == 'Report' || $page == 'Report Detail' ? 'active' : '';?>">
              <i class="nav-icon fas fa-envelope"></i>
              <p>
                Laporan
              </p>
            </a>
          </li>
          <li class="nav-item hidden-menu" id="sidebar-user-menu">
            <a href="<?=BASE_URL;?>user" class="nav-link <?=$page == 'User' ? 'active' : '';?>">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Pengguna
              </p>
            </a>
          </li>
          <li class="nav-item hidden-menu" id="sidebar-admin-menu">
            <a href="<?=BASE_URL;?>admin" class="nav-link <?=$page == 'Admin' ? 'active' : '';?>">
              <i class="nav-icon fas fa-user-shield"></i>
              <p>
                Admin
              </p>
            </a>
          </li>
          <li class="nav-item hidden-menu" id="sidebar-config-menu">
            <a href="<?=BASE_URL;?>config" class="nav-link <?=$page == 'Config' ? 'active' : '';?>">
              <i class="nav-icon fas fa-address-card"></i>
              <p>
                Tentang Kami
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
