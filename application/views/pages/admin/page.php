<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Admin</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=BASE_URL;?>dashboard">Dashboard</a></li>
            <li class="breadcrumb-item active">Admin</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="row d-flex justify-content-end mb-3">
                <button type="button" class="btn btn-success" id="admin-create-toggle" data-toggle="modal" data-target="#admin-create-modal">
                  <i class="fas fa-plus"></i> Buat
                </button>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="admin-datatable">
                  <thead>
                    <tr>
                      <th>Nama</th>
                      <th>Email</th>
                      <th>Tugas</th>
                      <th>Status</th>
                      <th>Tanggal Dibuat</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<!-- Modal -->
<div class="modal fade" id="admin-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Buat Admin</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="admin-create-form" autocomplete="off">          
        <div class="form-group">
          <label for="admin-fullname-create-field">Nama lengkap:</label>
          <input type="text" name="fullname" class="form-control" id="admin-fullname-create-field" placeholder="nama lengkap.." required>
        </div>
          <div class="form-group">
          <label for="admin-email-create-field">Email:</label>
          <input type="email" name="email" class="form-control" id="admin-email-create-field" placeholder="email.." autocomplete="off" required>
        </div>
        <div class="form-group">
          <label for="admin-password-create-field">Password:</label>
          <input type="password" name="password" class="form-control" id="admin-password-create-field" placeholder="password.." autocomplete="new-password" required>
        </div>
        <div class="form-group">
          <label for="admin-role-create-field">Tugas:</label>
          <select class="form-control" name="role" id="role-create-option" required>
            <option value="ADMIN">Admin</option>
            <!-- <option value="SALES">Sales</option> -->
          </select>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="admin-create-button">OK</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="admin-update-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="overlay" id="admin-update-overlay">
          <i class="fas fa-2x fa-sync fa-spin"></i>
      </div>
      <div class="modal-header">
        <h4 class="modal-title">Ubah Admin</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="admin-update-form">          
        <div class="form-group">
          <label for="admin-fullname-update-field">Nama Lengkap:</label>
          <input type="text" name="fullname" class="form-control" id="admin-fullname-update-field" placeholder="nama lengkap.." required>
        </div>
          <div class="form-group">
          <label for="admin-email-update-field">Email:</label>
          <input type="email" name="email" class="form-control" id="admin-email-update-field" placeholder="email.." required>
        </div>
        <div class="form-group">
          <label for="admin-role-update-field">Tugas:</label>
          <select class="form-control" name="role" id="role-update-option" required>
            <option value="ADMIN">Admin</option>
            <!-- <option value="SALES">Sales</option> -->
          </select>
        </div>
          <div class="form-group">
          <label for="admin-status-update-field">Status:</label>
          <span id="status-update-field"></span>
        </div>
        <div class="form-group">
          <label for="admin-createdAt-update-field">Tanggal Dibuat:</label>
          <input type="text" name="created_at" class="form-control" id="admin-created_at-update-field" placeholder="tanggal dibuat.." readonly>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="admin-update-button">OK</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="admin-inactive-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Non-aktif Admin</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin untuk menon-aktifkan akun admin ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-warning" id="admin-inactive-button">OK</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="admin-active-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Aktif Admin</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin untuk mengaktifkan akun admin ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-info" id="admin-active-button">OK</button>
      </div>
    </div>
  </div>
</div>