<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Tentang Kami</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=BASE_URL;?>dashboard">Dashboard</a></li>
            <li class="breadcrumb-item active">Tentang Kami</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title font-weight-bold"><i class="fas fa-address-card"></i> Tentang Kami</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="card-body">
              <form id="config-form">
                <div class="row">
                  <div class="col-12">
                    <div class="form-group">
                      <label>Deskripsi</label>
                      <textarea name="description" class="form-control" id="description" cols="30" rows="5" placeholder="Deskripsi Institusi atau aplikasi.." required></textarea>
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                      <label>Twitter</label>
                      <input type="text" class="form-control" id="twitter" name="twitter" placeholder="twitter.." required>
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                      <label>Instagram</label>
                      <input type="text" class="form-control" id="instagram" name="instagram" placeholder="instagram.." required>
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                      <label>Youtube</label>
                      <input type="text" class="form-control" id="youtube" name="youtube" placeholder="youtube.." required>
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                      <label>Whatsapp (Optional)</label>
                      <input type="text" class="form-control" id="whatsapp" name="whatsapp" placeholder="whatsapp..">
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                      <label for="config-logo-field">Logo 1:</label>
                      <p class="display-logo-1">
                      </p>
                      <button type="button" class="btn btn-dark btn-sm config-upload-logo" data-no="1" data-toggle="modal" data-target="#config-upload-logo-modal">
                        <i class="fas fa-upload"></i> Upload Logo 1
                      </button>
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                      <label for="config-icon-field">Logo 2:</label>
                      <p class="display-logo-2">
                      </p>
                      <button type="button" class="btn btn-dark btn-sm config-upload-logo" data-no="2" data-toggle="modal" data-target="#config-upload-logo-modal">
                        <i class="fas fa-upload"></i> Upload Logo 2
                      </button>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12">
                    <button type="submit" class="btn btn-success float-right" id="config-button">
                      <i class="fas fa-save"></i> Simpan
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title font-weight-bold"><i class="fas fa-image"></i> Foto</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="card-body">
              <div class="row mb-4">
                <button type="button" class="btn btn-success btn-lg" id="admin-create-toggle" data-toggle="modal" data-target="#config-upload-photo-modal">
                  <i class="fas fa-plus"></i> Tambah
                </button>
              </div>
              <div class="row" id="config-photos"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<div class="modal fade" id="config-upload-logo-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Upload Logo</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="config-upload-logo-form">          
        <div class="form-group">
          <label for="config-upload-logo-field">Logo:</label>
          <input type="file" class="form-control-file" id="config-upload-logo-field" name="logo" accept="image/*" required>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="config-upload-logo-button">OK</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="config-upload-photo-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Upload Foto</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="config-upload-photo-form">          
        <div class="form-group">
          <label for="config-uploadIcon-field">Foto:</label>
          <input type="hidden" name="type" value="PHOTO" required>
          <input type="file" class="form-control-file" id="config-upload-photo-field" name="file" accept="image/*" required>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="config-upload-photo-button">OK</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="config-delete-photo-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Foto</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin untuk menghapus foto tersebut?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-danger" id="config-delete-photo-button">OK</button>
        </form>
      </div>
    </div>
  </div>
</div>