<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Dashboard</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Dashboard</li>
          </ol>
        </div>
      </div>
    </div>
  </div>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-6">
          <!-- small box -->
          <div class="small-box bg-info">
            <div class="inner">
              <h3 id="dashboard-user-total">-</h3>
              <p>Pengguna</p>
            </div>
            <div class="icon">
            <i class="fas fa-user"></i>
            </div>
            <a href="<?=base_url('user');?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-6">
          <!-- small box -->
          <div class="small-box bg-primary">
            <div class="inner">
              <h3 id="dashboard-report-total">-</h3>
              <p>Laporan</p>
            </div>
            <div class="icon">
            <i class="fas fa-users"></i>
            </div>
            <a href="<?=base_url('report');?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>