<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>CMS Pomdam Jaya | Log in</title>
  <link rel="icon" href="<?=ASSETS;?>img/web/favicon.ico"/>
  
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Mochiy+Pop+P+One&family=Roboto+Condensed&display=swap" rel="stylesheet">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=ASSETS;?>third-party/adminlte/plugins/fontawesome-free/css/all.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="<?=ASSETS;?>third-party/adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=ASSETS;?>third-party/adminlte/dist/css/adminlte.min.css">
  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="<?=ASSETS;?>third-party/adminlte/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  <!-- Custom CSS -->
  <link rel="stylesheet" href="<?=ASSETS;?>css/login.css?v=<?=$version;?>">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo mb-4">
    <img src="<?=ASSETS;?>img/web/logo_sc.png" class="img-fluid" style="max-width:100px;">
    <img src="<?=ASSETS;?>img/web/logo_sc2.png" class="img-fluid" style="max-width:100px;"><br>
  </div>
  <div class="card shadow mb-5">
    <div class="card-body login-card-body" id="login-card">
      <h5 style="font-weight:bold; color:#00923F;" class="pb-2 text-center">POMDAM JAYA</h5>
      <form id="login-form">
        <div class="input-group mb-3">
          <input type="text" id="login-email-field" class="form-control" name="email" placeholder="Email" required autofocus>
          <div class="input-group-append" style="background-color:white;">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
           <input type="password" id="login-password-field" class="form-control" name="password" placeholder="Password" required>
          <div class="input-group-append" style="background-color:white;">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="text-center">
          <button type="submit" class="btn btn-block btn-success">
            Log in
          </button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- jQuery -->
<script src="<?=ASSETS;?>third-party/adminlte/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?=ASSETS;?>third-party/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=ASSETS;?>third-party/adminlte/dist/js/adminlte.min.js"></script>
<!-- SweetAlert2 -->
<script src="<?=ASSETS;?>third-party/adminlte/plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Custom JS -->
<script src="<?=ASSETS;?>js/login.js?v=<?=$version;?>"></script>
<script>
  const API_URL = "<?=API_URL;?>";
</script>
</body>
</html>
