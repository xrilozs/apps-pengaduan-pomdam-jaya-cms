<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12">
          <h1>Detail Laporan</h1>
        </div>
        <div class="col-sm-6">
          <a href="<?=base_url('report');?>"> <i class="fas fa-chevron-circle-left"></i> Kembali</a>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=base_url('dashboard');?>">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?=base_url('report');?>">Laporan</a></li>
            <li class="breadcrumb-item active">Detail Laporan</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content mb-4">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="overlay" id="report-form-overlay">
              <i class="fas fa-2x fa-sync fa-spin"></i>
            </div>
            <div class="card-body">
              <form id="report-form">
                <div class="row">
                  <div class="col-6">
                    <div class="form-group">
                      <label>Nama Lengkap</label>
                      <input type="text" name="fullname" class="form-control" readonly>
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                      <label>Judul Laporan</label>
                      <input type="text" name="title" class="form-control" readonly>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                      <label>Isi Laporan</label>
                      <textarea name="message" class="form-control" cols="30" rows="5" readonly></textarea>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                      <label>Lokasi</label>
                      <input type="text" name="location" class="form-control" readonly>
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                      <label>Longitude</label>
                      <input type="text" name="longitude" class="form-control" readonly>
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                      <label>Latitdude</label>
                      <input type="text" name="latitude" class="form-control" readonly>
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                      <label>Tanggal</label>
                      <input type="text" name="date" class="form-control" readonly>
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                      <label>Waktu</label>
                      <input type="text" name="time" class="form-control" readonly>
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                      <label>Status</label><br>
                      <span id="report-status"></span>
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                      <label>Tanggal Dibuat</label><br>
                      <span id="report-created-at"></span>
                    </div>
                  </div>
                </div>
              </form>
              <hr>
              <div id="report-photo">
                <h1>Foto</h1>
                <div id="report-photo-items" class="row">
                  <div class="col-12 d-flex justify-content-center">Belum ada Foto</div>
                </div>
              </div>
              <hr>
              <div id="report-video">
                <h1>Video</h1>
                <div id="report-video-items" class="row">
                  <div class="col-12 d-flex justify-content-center text-muted"><i>Belum ada Video</i></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row" id="report-response">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <form id="report-response-form">
                <div class="row">
                  <div class="col-12">
                    <div class="form-group">
                      <label>Tanggapan</label>
                      <textarea name="message" id="response-message" class="form-control" cols="30" rows="5" placeholder="Tanggapan Anda.." required></textarea>
                    </div>
                  </div>
                  <div class="col-12 d-flex flex-row-reverse" id="report-response-action">
                    <button type="submit" id="report-response-button" class="btn btn-primary">Beri Tanggapan</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>