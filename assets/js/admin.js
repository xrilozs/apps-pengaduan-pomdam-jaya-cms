let ADMIN_ID
$(document).ready(function(){
  //data table
  let admin_table = $('#admin-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url: `${ADMIN_API_URL}`,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.draw = d.draw
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          { 
            data: "fullname",
            orderable: false
          },
          { 
            data: "email",
            orderable: false
          },
          { 
            data: "role",
            orderable: false
          },
          {
            data: "is_active",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              const is_active = parseInt(data)
              let color = is_active ? 'primary' : 'warning'
              let status = is_active ? 'Aktif' : 'Tidak Aktif'
              let badge = `<span class="badge badge-pill badge-${color}">${status}</span>`

              return badge
            }
          },
          {
            data: "created_at",
            orderable: false
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              const is_active = parseInt(row.is_active)

              let button = `
              <button class="btn btn-sm btn-primary admin-update-toggle" data-id="${data}" data-toggle="modal" data-target="#admin-update-modal" title="update">
                <i class="fas fa-edit"></i>
              </button>`
              if(is_active){
                button += ` <button class="btn btn-sm btn-warning admin-inactive-toggle" data-id="${data}" data-toggle="modal" data-target="#admin-inactive-modal" title="inactive">
                  <i class="fas fa-ban"></i>
                </button>`
              }else{
                button += ` <button class="btn btn-sm btn-info admin-active-toggle" data-id="${data}" data-toggle="modal" data-target="#admin-active-modal" title="active">
                  <i class="fas fa-check"></i>
                </button>`
              }
              
              return button
            },
            orderable: false
          }
      ]
  });
  
  //toggle
  $('#admin-create-toggle').click(function(e) {
    clearForm('create')  
  })

  $("body").delegate(".admin-detail-toggle", "click", function(e) {
    ADMIN_ID = $(this).data('id')
    clearForm("detail")
    $('#admin-detail-overlay').show()

    $.ajax({
        async: true,
        url: `${ADMIN_API_URL}by-id/${ADMIN_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#admin-detail-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          renderForm(response, 'detail')
          $('#admin-detail-overlay').hide()
        }
    });
  })
  
  $("body").delegate(".admin-update-toggle", "click", function(e) {
    ADMIN_ID = $(this).data('id')
    clearForm("update")
    $('#admin-update-overlay').show()

    $.ajax({
        async: true,
        url: `${ADMIN_API_URL}by-id/${ADMIN_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#admin-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          renderForm(response, 'update')
          $('#admin-update-overlay').hide()
        }
    });
  })
  
  $("body").delegate(".admin-inactive-toggle", "click", function(e) {
    ADMIN_ID = $(this).data('id')
  })
  
  $("body").delegate(".admin-active-toggle", "click", function(e) {
    ADMIN_ID = $(this).data('id')
  })

  //form
  $('#admin-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#admin-create-button')
    
    let $form = $(this),
        request = {
          password: $form.find( "input[name='password']" ).val(),
          email: $form.find( "input[name='email']" ).val(),
          fullname: $form.find( "input[name='fullname']" ).val(),
          role: $('#role-create-option').find(":selected").val()
        }
        
    $.ajax({
        async: true,
        url: ADMIN_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#admin-create-button', 'OK')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#admin-create-button', 'OK')
          $('#admin-create-modal').modal('hide')
          admin_table.ajax.reload()
        }
    });
  })
  
  $('#admin-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#admin-update-button')
    
    let $form = $(this),
        request = {
          id: ADMIN_ID,
          password: $form.find( "input[name='password']" ).val(),
          email: $form.find( "input[name='email']" ).val(),
          fullname: $form.find( "input[name='fullname']" ).val(),
          role: $('#role-update-option').find(":selected").val()
        }

    $.ajax({
        async: true,
        url: ADMIN_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#admin-update-button', 'OK')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#admin-update-button', 'OK')
          $('#admin-update-modal').modal('toggle')
          admin_table.ajax.reload()
        }
    });
  })
  
  $('#admin-inactive-button').click(function (){
    startLoadingButton('#admin-inactive-button')
    
    $.ajax({
        async: true,
        url: `${ADMIN_API_URL}inactive/${ADMIN_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#admin-inactive-button', 'OK')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#admin-inactive-button', 'OK')
          $('#admin-inactive-modal').modal('toggle')
          admin_table.ajax.reload()
        }
    });
  })
  
  $('#admin-active-button').click(function (){
    startLoadingButton('#admin-active-button')
    
    $.ajax({
        async: true,
        url: `${ADMIN_API_URL}active/${ADMIN_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#admin-active-button', 'OK')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#admin-active-button', 'OK')
          $('#admin-active-modal').modal('toggle')
          admin_table.ajax.reload()
        }
    });
  })
})

function renderForm(data, type){
  let $form = $(`#admin-${type}-form`)
  $form.find( "input[name='email']" ).val(data.email)
  $form.find( "input[name='fullname']" ).val(data.fullname)
  $(`#role-${type}-option`).val(data.role).change()

  const is_active = parseInt(data.is_active)
  const status = is_active ? 'AKTIF' : 'TIDAK AKTIF'
  let color = is_active ? 'primary' : 'warning'
  let badge = `<span class="badge badge-pill badge-${color}">${status}</span>`
  $(`#status-${type}-field`).html(badge)
  
  if(type == 'update'){
    $form.find( "input[name='created_at']" ).val(data.created_at)
  }
}

function clearForm(type){
  let $form = $(`#admin-${type}-form`)
  if(type == 'create'){
    $form.find( "input[name='password']" ).val("")
  }
  $form.find( "input[name='email']" ).val("")
  $form.find( "input[name='fullname']" ).val("")
}