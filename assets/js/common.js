let CONFIG_API_URL        = `${API_URL}config/`
let REPORT_API_URL        = `${API_URL}report/`
let NOTIFICATION_API_URL  = `${API_URL}notification/`
let ADMIN_API_URL         = `${API_URL}admin/`
let USER_API_URL          = `${API_URL}user/`
let TOAST                 = Swal.mixin({
                              toast: true,
                              position: 'top-end',
                              showConfirmButton: false,
                              timer: 3000
                            });
                          

let SESSION = localStorage.getItem("user-token")
let REFRESH_SESSION = localStorage.getItem("user-refresh-token")
let ADMIN_ROLE = localStorage.getItem("user-role")
let ADMIN_FULLNAME = localStorage.getItem("user-fullname")
let RETRY_COUNT = 0

$('.rich-text-create').summernote({
  placeholder: 'Type here..',
  height: 300
});
$('.rich-text-update').summernote({
  placeholder: 'Type here..',
  height: 300
});

$(document).ready(function(){
  if(!SESSION){
    window.location.href = `${WEB_URL}login`
  }else{
    checkAuthorityPage()
  }
  
  renderHeader()
  renderSidebar()
});

function getWebUrl(){
  let base_url = window.location.origin;
  return base_url.includes("localhost") ? "https://localhost/rakernas-cms/" : "https://cms.rakernispuspomad.com/"
}

function renderHeader(){
  $('#header-adminName').html(`${ADMIN_FULLNAME} (${ADMIN_ROLE})`)
}

function renderSidebar(){
  // if(ADMIN_ROLE == 'SUPERADMIN'){
    $(".hidden-menu").css("display", "block")
  // }else if(ADMIN_ROLE == 'ADMIN'){
  //   $("#sidebar-rakernis-menu").css("display", "block")
  //   $("#sidebar-materi-menu").css("display", "block")
  //   $("#sidebar-absensi-menu").css("display", "block")
  //   $("#sidebar-reviews-menu").css("display", "block")
  // }
}

function startLoadingButton(dom){
  let loading_button = `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>`
  $(dom).html(loading_button)
  $(dom).prop('disabled', true);
}

function endLoadingButton(dom, text){
  $(dom).html(text)
  $(dom).prop('disabled', false);
}

function showError(message){
  TOAST.fire({
    icon: 'error',
    title: message
  })
}

function showSuccess(message){
  TOAST.fire({
    icon: 'success',
    title: message
  })
}

function showSuccessV2(title, text, confirmLink=null){
  Swal.fire({
      icon: 'success',
      title: title,
      text: text,
      confirmButtonColor: '#00923F'
  }).then((result) => {
      if (confirmLink) {
          window.location.href = confirmLink
      }
  }); 
}

function setSession(data){
  console.log("SET NEW SESSION")
  localStorage.setItem("user-token", data.accessToken)
  localStorage.setItem("user-refresh-token", data.refreshToken)
  localStorage.setItem("user-fullname", data.fullname);
  localStorage.setItem("user-role", data.role);
  SESSION = data.accessToken
  REFRESH_SESSION = data.refreshToken
}

function removeSession(){
  console.log("REMOVE SESSION")
  localStorage.removeItem("user-token");
  localStorage.removeItem("user-refresh-token");
  localStorage.removeItem("user-fullname");
  localStorage.removeItem("user-role");
  window.location.href = `${WEB_URL}login`
}

function refreshToken(){
  let resp = {}
  $.ajax({
      async: false,
      url: `${API_URL}admin/refresh`,
      type: 'GET',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${REFRESH_SESSION}`);
      },
      error: function(res) {
        const response = JSON.parse(res.responseText)
        resp = {status: "failed"}
      },
      success: function(res) {
        const response = res.data
        resp = {status: "success", data: response}
      }
  });
  
  return resp
}

function retryRequest(responseError){
  if(responseError.code == 401){
    if(RETRY_COUNT < 3){
      let resObj = refreshToken()          
      if(resObj.status == 'success'){
        RETRY_COUNT += 1 
        setSession(resObj.data)
        return true
      }else if(resObj.status == 'failed'){
        removeSession()
      }
    }else{
      removeSession()
    }
  }else{
    showError(responseError.message)
    return false
  }
}

function checkAuthorityPage(){
  // if(ADMIN_ROLE == 'ADMIN' || ADMIN_ROLE == 'SUPERADMIN'){
      // window.location.href = `dashboard`
  // }
}

function formatRupiah(angka, prefix){
  var angkaStr = angka.replace(/[^,\d]/g, '').toString(),
      split = angkaStr.split(','),
      sisa = split[0].length % 3,
      rupiah = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

  // tambahkan titik jika yang di input sudah menjadi angka ribuan
  if(ribuan){
    separator = sisa ? '.' : '';
    rupiah += separator + ribuan.join('.');
  }

  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}

function formatDate(datetimeStr){
  let datetimeArr = datetimeStr.split(" ")
  let dateStr = datetimeArr[0]
  let arr = dateStr.split("-")
  return `${arr[2]}-${arr[1]}-${arr[0]}`
}

function convertDate(dateObj){
  const month = dateObj.getMonth()+1;
  const day = String(dateObj.getDate()).padStart(2, '0');
  const year = dateObj.getFullYear();
  const output = day  + '-'+ month  + '-' + year;
  return output
}

$("#logout-button").click(function(){    
  removeSession()
})

$("#changePassword-toggle").click(function(e){
  let $form = $("#changePassword-form" )
  $form.find( "input[name='old_password']" ).val('')
  $form.find( "input[name='new_password']" ).val('')
})

$("#changePassword-form").submit(function(e){
  e.preventDefault()
  startLoadingButton("#changePassword-button")

  let $form = $( this ),
      old_password = $form.find( "input[name='old_password']" ).val(),
      new_password = $form.find( "input[name='new_password']" ).val()
  
  $.ajax({
      async: true,
      url: `${API_URL}admin/change-password`,
      type: 'PUT',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: JSON.stringify({
        old_password: old_password, 
        new_password: new_password
      }),
      error: function(res) {
        const response = JSON.parse(res.responseText)
        showError(response.message)
        endLoadingButton('#changePassword-button', 'OK')
        let is_retry = retryRequest(response)
        if(is_retry) $.ajax(this)
      },
      success: function(res) {
        showSuccess(res.message)
        endLoadingButton('#changePassword-button', 'OK')
        $('#changePassword-modal').modal('hide')
      }
  });
})

