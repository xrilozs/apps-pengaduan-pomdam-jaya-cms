let CONFIG_ID;
let LOGO1_URL;
let LOGO2_URL;
let FULL_LOGO1_URL;
let FULL_LOGO2_URL;
let LOGO_NO;
let CONFIG_ATTACHMENT_ID;

$(document).ready(function(){
  getConfig()
  
  $('#config-form').submit(function (e){
    e.preventDefault();
    startLoadingButton("#config-button")
    
    let $form = $( this ),
        request = {
          id: CONFIG_ID,
          youtube: $form.find( "input[name='youtube']" ).val(),
          twitter: $form.find( "input[name='twitter']" ).val(),
          instagram: $form.find( "input[name='instagram']" ).val(),
          whatsapp: $form.find( "input[name='whatsapp']" ).val(),
          description: $form.find( "textarea[name='description']" ).val(),
          logo_1: LOGO1_URL,
          logo_2: LOGO2_URL,
        }

    $.ajax({
        async: true,
        url: `${CONFIG_API_URL}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#config-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#config-button', 'Simpan')
          showSuccess(res.message)
          getConfig()
        }
    });
  })

  //upload image
  $('#config-upload-logo-form').submit(function(e){
    e.preventDefault()
    startLoadingButton('#config-upload-logo-button')
    
    $.ajax({
        async: true,
        url: `${CONFIG_API_URL}upload-logo`,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: new FormData($('#config-upload-logo-form')[0]),
        processData: false, 
        contentType: false, 
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#config-upload-logo-button', 'OK')
        },
        success: function(res) {
          const response = res.data
          if(LOGO_NO == 1){
            LOGO1_URL = response.url
            FULL_LOGO1_URL = response.full_url
          }else{
            LOGO2_URL = response.url
            FULL_LOGO2_URL = response.full_url
          }
          endLoadingButton('#config-upload-logo-button', 'OK')
          $('#config-upload-logo-modal').modal('hide')
          // showSuccess(res.message)
          renderLogo()
        }
    });
  });

  $('#config-upload-photo-form').submit(function(e){
    e.preventDefault()
    startLoadingButton('#config-upload-photo-button')
    
    $.ajax({
        async: true,
        url: `${CONFIG_API_URL}upload-attachment`,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: new FormData($('#config-upload-photo-form')[0]),
        processData: false, 
        contentType: false, 
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#config-upload-photo-button', 'OK')
        },
        success: function(res) {
          endLoadingButton('#config-upload-photo-button', 'OK')
          $('#config-upload-photo-modal').modal('hide')

          showSuccess(res.message)
          getConfig()
        }
    });
  });

  $('#config-delete-photo-button').click(function(e){
    e.preventDefault()
    startLoadingButton('#config-delete-photo-button')
    
    $.ajax({
        async: true,
        url: `${CONFIG_API_URL}remove-attachment/${CONFIG_ATTACHMENT_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#config-delete-photo-button', 'OK')
        },
        success: function(res) {
          endLoadingButton('#config-delete-photo-button', 'OK')
          $('#config-delete-photo-modal').modal('hide')

          showSuccess(res.message)
          getConfig()
        }
    });
  });

  $('.config-upload-logo').click(function(){
    LOGO_NO = parseInt($(this).data('no'))
  })

  $("body").delegate(".config-delete-photo", "click", function(e) {
    CONFIG_ATTACHMENT_ID = $(this).data('id')
  })
})

function getConfig(){
  $.ajax({
    async: true,
    url: CONFIG_API_URL,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      CONFIG_ID = res.data.id
      renderConfigForm(res.data)
      renderConfigPhotos(res.data.photo)
    }
  });
}

function renderConfigForm(data){
  let $form = $(`#config-form`)
  $form.find( "input[name='youtube']" ).val(data.youtube)
  $form.find( "input[name='twitter']" ).val(data.twitter)
  $form.find( "input[name='instagram']" ).val(data.instagram)
  $form.find( "input[name='whatsapp']" ).val(data.whatsapp)
  $form.find( "textarea[name='description']" ).val(data.description)

  LOGO1_URL = data.logo_1
  LOGO2_URL = data.logo_2
  if(LOGO1_URL){
    let logo_html = `<img src="${API_URL + LOGO1_URL}" class="img-fluid" style="max-width:100px;">`
    $(`.display-logo-1`).html(logo_html)
  }

  if(LOGO2_URL){
    let logo_html = `<img src="${API_URL + LOGO2_URL}" class="img-fluid" style="max-width:100px;">`
    $(`.display-logo-2`).html(logo_html)
  }
}

function renderConfigPhotos(data){
  let photos_html = ""
  data.forEach(item => {
    const item_html = `<div class="col-lg-2 col-md-3 col-sm-6">
      <div class="row">
        <div class="col-12 mb-4 d-flex justify-content-center">
          <img src="${item.full_url}" class="img-fluid" style="max-width:100px;">
        </div>
        <div class="col-12 d-flex justify-content-center">
          <button class="btn btn-sm btn-danger config-delete-photo" data-id="${item.id}" data-toggle="modal" data-target="#config-delete-photo-modal">
            <i class="fas fa-trash"></i> Hapus
          </button>
        </div>
      </div>
    </div>`
    photos_html += item_html
  });

  $('#config-photos').html(photos_html)
}

function renderLogo(){
  let url = LOGO_NO == 1 ? FULL_LOGO1_URL : FULL_LOGO2_URL
  let img_html = `<img src="${url}" class="img-fluid" style="max-width:100px;">`
  $(`.display-logo-${LOGO_NO}`).html(img_html)
}
