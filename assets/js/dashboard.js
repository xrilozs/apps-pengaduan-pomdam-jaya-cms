$(document).ready(function(){ 
  getUserTotal()
  getReportTotal()
});

function getUserTotal(){
  $.ajax({
    async: true,
    url: `${USER_API_URL}count`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      showError(response.message)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const response = res.data
      $('#dashboard-user-total').html(response)
    }
  });
}

function getReportTotal(){
  $.ajax({
    async: true,
    url: `${REPORT_API_URL}count?status=SENT`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      showError(response.message)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const response = res.data
      $('#dashboard-report-total').html(response)
    }
  });
}
