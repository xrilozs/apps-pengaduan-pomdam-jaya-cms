let TOAST = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});
let SESSION = localStorage.getItem("user-token");
let ADMIN_ROLE = localStorage.getItem("user-role");

$(document).ready(function(){
  if(SESSION){ 
    window.location.href = 'dashboard'
  }

  $( "#login-form" ).submit(function( e ) {
    e.preventDefault();
    startLoadingButton("#login-button")
  
    let $form = $( this ),
    email = $form.find( "input[name='email']" ).val(),
    password = $form.find( "input[name='password']" ).val()
    
    $.ajax({
        async: true,
        url: `${API_URL}admin/login`,
        type: 'POST',
        data: JSON.stringify({
          email: email,
          password: password
        }),
        error: function(res) {
          response = res.responseJSON
          showError(response.message)
          endLoadingButton('#login-button', 'Log In')
        },
        success: function(res) {
          response = res.data;
          setSession(response)
          window.location.href = 'dashboard'
        }
    });    
  })
})

function startLoadingButton(dom){
  let loading_button = `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>`
  $(dom).html(loading_button)
  $(dom).prop('disabled', true);
}

function endLoadingButton(dom, text){
  $(dom).html(text)
  $(dom).prop('disabled', false);
}

function showError(message){
  TOAST.fire({
    icon: 'error',
    title: message
  })
}

function showSuccess(message){
  TOAST.fire({
    icon: 'success',
    title: message
  })
}

function setSession(data){
  console.log("SET NEW SESSION")
  localStorage.setItem("user-token", data.accessToken)
  localStorage.setItem("user-refresh-token", data.refreshToken)
  localStorage.setItem("user-fullname", data.fullname);
  localStorage.setItem("user-role", data.role);
  SESSION = data.accessToken
  REFRESH_SESSION = data.refreshToken
  ADMIN_FULLNAME = data.fullname
  ADMIN_ROLE = data.role
}