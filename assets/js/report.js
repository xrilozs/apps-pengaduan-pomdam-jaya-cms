let REPORT_ID


$(document).ready(function(){
  $('#event-date-create').datetimepicker({
    format: 'L'
  });
  $('#event-date-update').datetimepicker({
    format: 'L'
  });

  //render datatable
  let rakernis_table = $('#report-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ordering: false,
      ajax: {
        async: true,
        url: REPORT_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.draw = d.draw
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          {
            data: "fullname",
          },
          { 
            data: "title",
          },
          { 
            data: "message",
          },
          { 
            data: "location",
          },
          { 
            data: "date",
            render: function (data, type, row, meta) {
              return `${data} ${row.time}`
            }
          },
          {
            data: "status",
            render: function (data, type, row, meta) {
              let color = data == 'SENT' ? 'dark' : 'success'
              let status = data == 'SENT' ? 'Belum Ditanggapi' : 'Sudah Ditanggapi'
              let badge = `<span class="badge badge-pill badge-${color}">${status}</span>`

              return badge
            },
          },
          {
            data: "created_at",
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
                let actions = `<a href="report/detail/${data}" class="btn btn-sm btn-default" title="detail">
                  <i class="fas fa-search"></i>
                </a>`

                return actions
            },
          }
      ]
  });
})
