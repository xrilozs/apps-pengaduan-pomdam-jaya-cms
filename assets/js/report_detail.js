let REPORT_ID = window.location.pathname.split("/").pop()

$(document).ready(function(){
  getReportDetail()

  $('#report-response-form').submit(function(e){
    e.preventDefault();
    startLoadingButton('#report-response-button')
    
    let $form = $(this),
        request = {
          report_id: REPORT_ID,
          title: "Pengaduan Diterima",
          message: $form.find( "textarea[name='message']" ).val(),
        }
        
    $.ajax({
        async: true,
        url: `${REPORT_API_URL}respond`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#report-response-button', 'OK')
        },
        success: function(res) {
          showSuccessV2("Laporan", res.message, `${WEB_URL}report`)
        }
    });
  })
})

function getReportDetail(){
  $.ajax({
    async: true,
    url: `${REPORT_API_URL}detail/${REPORT_ID}`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const response = res.data
      $('#report-form-overlay').hide()
      renderForm(response)
      renderPhotos(response.photo)
      renderVideos(response.video)
    }
  });
}

function renderForm(data){
  let $form = $(`#report-form`)
  $form.find( "input[name='fullname']" ).val(data.fullname)
  $form.find( "input[name='title']" ).val(data.title)
  $form.find( "textarea[name='message']" ).val(data.message)
  $form.find( "input[name='location']" ).val(data.location)
  $form.find( "input[name='longitude']" ).val(data.longitude)
  $form.find( "input[name='latitude']" ).val(data.latitude)
  $form.find( "input[name='date']" ).val(data.date)
  $form.find( "input[name='time']" ).val(data.time)

  let color = data.status == 'SENT' ? 'dark' : 'success'
  let status = data.status == 'SENT' ? 'Belum Ditanggapi' : 'Sudah Ditanggapi'
  let badge = `<span class="badge badge-pill badge-${color}">${status}</span>`
  $('#report-status').html(badge)
  $('#report-created-at').html(data.created_at)

  if(data.status == 'RESPONDED'){
    const notification = data.notification_detail
    $('#report-response-action').attr('style', 'display: none !important')
    $('#response-message').val(notification.message)
    $('#response-message').prop("readonly", true)
  }
}

function renderPhotos(items){
  let photos_html = ""
  items.forEach(item => {
    const item_html = `<div class="col-lg-3 col-md-4 col-sm-6  d-flex justify-content-center">
      <img src="${item.full_url}" class="img-fluid" style="max-width:200px;">
    </div>`
    photos_html += item_html
  });

  $('#report-photo-items').html(photos_html)
}

function renderVideos(items){
  let videos_html = ""
  items.forEach(item => {
    const item_html = `<div class="col-12 d-flex justify-content-center">
      <video width="400" controls>
        <source src="${item.full_url}">
        Your browser does not support HTML video.
      </video>
    </div>`
    videos_html += item_html
  });

  $('#report-video-items').html(videos_html)
}