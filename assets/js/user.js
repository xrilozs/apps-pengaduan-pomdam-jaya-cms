let USER_ID
$(document).ready(function(){
  //data table
  let user_table = $('#user-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url: `${USER_API_URL}`,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.draw = d.draw
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          { 
            data: "fullname",
            orderable: false
          },
          { 
            data: "email",
            orderable: false
          },
          { 
            data: "handphone",
            orderable: false
          },
          {
            data: "status",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              let color = 'default'
              let status = 'Sedang melakukan verifikasi'
              
              if(data == 'ACTIVE'){
                color = 'primary'
                status = 'Aktif'
              }else if (data == 'INACTIVE'){
                color = 'danger'
                status = 'Non-aktif'
              }
              
              let badge = `<span class="badge badge-pill badge-${color}">${status}</span>`

              return badge
            }
          },
          {
            data: "created_at",
            orderable: false
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              let button = `<button class="btn btn-sm btn-light user-detail-toggle" data-id="${data}" data-toggle="modal" data-target="#user-detail-modal" title="detail">
                <i class="fas fa-search"></i>
              </button>`
              if(row.status == 'ACTIVE'){
                button += ` <button class="btn btn-sm btn-warning user-inactive-toggle" data-id="${data}" data-toggle="modal" data-target="#user-inactive-modal" title="inactive">
                  <i class="fas fa-ban"></i>
                </button>`
              }else{
                button += ` <button class="btn btn-sm btn-info user-active-toggle" data-id="${data}" data-toggle="modal" data-target="#user-active-modal" title="active">
                  <i class="fas fa-check"></i>
                </button>`
              }
              
              return button
            },
            orderable: false
          }
      ]
  });
  
  //toggle
  $("body").delegate(".user-detail-toggle", "click", function(e) {
    USER_ID = $(this).data('id')
    clearForm("detail")
    $('#user-detail-overlay').show()

    $.ajax({
        async: true,
        url: `${USER_API_URL}by-id/${USER_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#user-detail-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          renderForm(response, 'detail')
          $('#user-detail-overlay').hide()
        }
    });
  })
  
  $("body").delegate(".user-inactive-toggle", "click", function(e) {
    USER_ID = $(this).data('id')
  })
  
  $("body").delegate(".user-active-toggle", "click", function(e) {
    USER_ID = $(this).data('id')
  })

  //action  
  $('#user-inactive-button').click(function (){
    startLoadingButton('#user-inactive-button')
    
    $.ajax({
        async: true,
        url: `${USER_API_URL}inactive/${USER_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#user-inactive-button', 'OK')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#user-inactive-button', 'OK')
          $('#user-inactive-modal').modal('toggle')
          user_table.ajax.reload()
        }
    });
  })
  
  $('#user-active-button').click(function (){
    startLoadingButton('#user-active-button')
    
    $.ajax({
        async: true,
        url: `${USER_API_URL}active/${USER_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#user-active-button', 'OK')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#user-active-button', 'OK')
          $('#user-active-modal').modal('toggle')
          user_table.ajax.reload()
        }
    });
  })
})

function renderForm(data, type){
  let $form = $(`#user-${type}-form`)
  $form.find( "input[name='handphone']" ).val(data.handphone)
  $form.find( "input[name='email']" ).val(data.email)
  $form.find( "input[name='fullname']" ).val(data.fullname)

  let color = 'default'
  let status = 'Sedang melakukan verifikasi'
  
  if(data.status == 'ACTIVE'){
    color = 'primary'
    status = 'Aktif'
  }else if (data.status == 'INACTIVE'){
    color = 'danger'
    status = 'Non-aktif'
  }
  
  let badge = `<span class="badge badge-pill badge-${color}">${status}</span>`
  $(`#user-status-${type}-field`).html(badge)
}

function clearForm(type){
  console.log("CLEAR FORM ", type)
  let $form = $(`#user-${type}-form`)
  $form.find( "input[name='handphone']" ).val("")
  $form.find( "input[name='email']" ).val("")
  $form.find( "input[name='fullname']" ).val("")
}